package fr.cedricalibert.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import fr.cedricalibert.hibernate.demo.entity.Course;
import fr.cedricalibert.hibernate.demo.entity.Instructor;
import fr.cedricalibert.hibernate.demo.entity.InstructorDetail;

public class EagerLazyDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			//start a transaction
			session.beginTransaction();
			
			
			//get instructor
			int id = 1;
			/*
			 * // solve lazy exception session close : option 1 getData before session close
			 * 
			 * 
			Instructor instructor = session.get(Instructor.class, id);
			
			System.out.println("Instructor " + instructor);
			
			
			
			
			
			
			//get instructor courses
			System.out.println("Instructor courses : " +instructor.getCourses());
			
			*/
			
			//option 2 : query with HQL
			Query<Instructor> query = 
					session.createQuery("select i from Instructor i "
							+ "join fetch i.courses "
							+ "where i.id = :id",
							Instructor.class);
			
			query.setParameter("id", id);
			
			Instructor instructor = query.getSingleResult();
			
			System.out.println("Instructor " + instructor);
			
			//commit transaction
			session.getTransaction().commit();
			
			session.close();
			System.out.println("Session is now close");
			
			//
			
			//get instructor courses
			System.out.println("Instructor courses : " +instructor.getCourses());
		
			
			System.out.println("Done");
			
		}
		finally {
			session.close();
			factory.close();
		}

	}

}
